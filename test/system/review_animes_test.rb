require "application_system_test_case"

class ReviewAnimesTest < ApplicationSystemTestCase
  setup do
    @review_anime = review_animes(:one)
  end

  test "visiting the index" do
    visit review_animes_url
    assert_selector "h1", text: "Review Animes"
  end

  test "creating a Review anime" do
    visit review_animes_url
    click_on "New Review Anime"

    fill_in "Comment", with: @review_anime.comment
    fill_in "Rating", with: @review_anime.rating
    click_on "Create Review anime"

    assert_text "Review anime was successfully created"
    click_on "Back"
  end

  test "updating a Review anime" do
    visit review_animes_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @review_anime.comment
    fill_in "Rating", with: @review_anime.rating
    click_on "Update Review anime"

    assert_text "Review anime was successfully updated"
    click_on "Back"
  end

  test "destroying a Review anime" do
    visit review_animes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Review anime was successfully destroyed"
  end
end
