require "application_system_test_case"

class ReviewGamesTest < ApplicationSystemTestCase
  setup do
    @review_game = review_games(:one)
  end

  test "visiting the index" do
    visit review_games_url
    assert_selector "h1", text: "Review Games"
  end

  test "creating a Review game" do
    visit review_games_url
    click_on "New Review Game"

    fill_in "Comment", with: @review_game.comment
    fill_in "Rating", with: @review_game.rating
    click_on "Create Review game"

    assert_text "Review game was successfully created"
    click_on "Back"
  end

  test "updating a Review game" do
    visit review_games_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @review_game.comment
    fill_in "Rating", with: @review_game.rating
    click_on "Update Review game"

    assert_text "Review game was successfully updated"
    click_on "Back"
  end

  test "destroying a Review game" do
    visit review_games_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Review game was successfully destroyed"
  end
end
