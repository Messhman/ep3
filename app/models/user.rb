class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :reviews, dependent: :destroy
  has_many :review_animes, dependent: :destroy
  has_many :review_books, dependent: :destroy
  has_many :review_games, dependent: :destroy
  has_many :review_mangas, dependent: :destroy
end
