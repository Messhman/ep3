json.extract! review_book, :id, :rating, :comment, :created_at, :updated_at
json.url review_book_url(review_book, format: :json)
