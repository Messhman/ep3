class ReviewGamesController < ApplicationController
  before_action :set_review_game, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_game

  # GET /review_games/new
  def new
    @review_game = ReviewGame.new
  end

  # GET /review_games/1/edit
  def edit
  end

  # POST /review_games
  # POST /review_games.json
  def create
    @review_game = ReviewGame.new(review_game_params)
    @review_game.user_id = current_user.id
    @review_game.game_id = @game.id

    respond_to do |format|
      if @review_game.save
        format.html { redirect_to @game, notice: 'Análise feita com sucesso.' }
        format.json { render :show, status: :created, location: @review_game }
      else
        format.html { render :new }
        format.json { render json: @review_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /review_games/1
  # PATCH/PUT /review_games/1.json
  def update
    respond_to do |format|
      if @review_game.update(review_game_params)
        format.html { redirect_to @game, notice: 'Análise editada com sucesso.' }
        format.json { render :show, status: :ok, location: @review_game }
      else
        format.html { render :edit }
        format.json { render json: @review_game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /review_games/1
  # DELETE /review_games/1.json
  def destroy
    @review_game = ReviewGame.find(params[:id])
    @review_game.destroy!
    redirect_to @game, notice: 'Análise deletada com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review_game
      @review_game = ReviewGame.find(params[:id])
    end

    def set_game
      @game = Game.find(params[:game_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_game_params
      params.require(:review_game).permit(:rating, :comment)
    end
end
