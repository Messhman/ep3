class RemoveAnimeIdFromReview < ActiveRecord::Migration[5.2]
  def up
    remove_column :reviews, :anime_id, :integer
  end

  def down
    add_column :reviews, :anime_id, :integer
  end
end
