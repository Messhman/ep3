class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.text :synopsis
      t.string :length
      t.string :director
      t.string :genre
      t.string :rating
      t.text :release_date

      t.timestamps
    end
  end
end
