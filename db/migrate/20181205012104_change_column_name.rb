class ChangeColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :reviews, :rating, :score
  end
end
