class AddReleaseDateToAnimes < ActiveRecord::Migration[5.2]
  def change
    add_column :animes, :release_date, :string
  end
end
