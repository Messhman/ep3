class CreateAnimes < ActiveRecord::Migration[5.2]
  def change
    create_table :animes do |t|
      t.string :title
      t.text :synopsis
      t.string :rating
      t.string :genre
      t.string :prequel
      t.string :sequel

      t.timestamps
    end
  end
end
