class DropRatesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :rates
    drop_table :average_caches
    drop_table :overall_averages
    drop_table :admins
  end

  def down
    create_table :rates
    create_table :average_caches
    create_table :overall_averages
    create_table :admins
  end
end
