class AddGameIdToReviewGames < ActiveRecord::Migration[5.2]
  def change
    add_column :review_games, :game_id, :integer
  end
end
