class AddUserIdToReviewBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :review_books, :user_id, :integer
  end
end
